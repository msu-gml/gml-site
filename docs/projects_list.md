## Id и названия проектов (для быстрого редактирования связей):

Если проекта в списке нет, то инструкция по быстрому добавлению проекта находится в [документе](./docs/add_project.md).

Для автоматического обновления списка нужно:
1. Зайти в папку `scripts`.
2. Запустить скрипт `update_docs_ids.py`

| Id | Project Name |
|---|---|
| traffic_sign_recognition | Traffic sign recognition |
| video_matting_benchmark | Video Matting Benchmark |
| human_gait_recognition_in_video | Human gait recognition in video |
| medical_image_analysis | Medical image analysis |
| traffic_sign_recognition_with_synthetic_datasets | Traffic sign recognition with synthetic datasets |
| text_detection_and_recognition_in_natural_images | Text detection and recognition in natural images |
| geometric_image_parsing_in_man_made_environments | Geometric image parsing in man-made environments |
