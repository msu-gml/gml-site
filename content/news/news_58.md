Active: yes
Title: We congratulate Olga Senyukova with successful defense of PhD
Category: news
Date: 2012-10-24 00:00

We congratulate Olga Senyukova with successful defense of PhD Thesis "Algorithms for semantic image segmentation and classification of biomedical signals based on machine learning”. Thesis summary can be found [here](http://cmc.msu.ru/sites/cmc/files/theses/201209-sov.pdf)
