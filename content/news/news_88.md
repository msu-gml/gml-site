Active: yes
Title: Congratulations to Olga Senyukova, who has received CMC scholarship for young scientists and teachers
Category: news
Date: 2014-01-09 00:00

Congratulations to Olga Senyukova, who has received CMC scholarship for young scientists and teachers who achieved significant results in teaching and research activities.
