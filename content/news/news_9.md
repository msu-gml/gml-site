Active: yes
Title: Сюжет о лаборатории в программе "Вести-Москва"
Category: news
Date: 2009-10-12 00:00

Наш стенд на прошедшем IV Фестивале науки попал в объектив телекамер программы "Вести-Москва".

[![Сюжет о лаборатории в программе "Вести-Москва"]({static}/images/news/sjuzhet_o_laboratorii_v_programme_0.png "Сюжет о лаборатории в программе "Вести-Москва"")](http://www.vesti-moscow.ru/videos.html?id=47252&type=r)

Видео можно посмотреть на [сайте программы](http://www.vesti-moscow.ru/videos.html?id=47252&type=r).

![Сюжет о лаборатории в программе "Вести-Москва"]({static}/images/news/sjuzhet_o_laboratorii_v_programme_1.png "Сюжет о лаборатории в программе "Вести-Москва"")

Так же можно прочитать [текстовую заметку](http://www.vesti-moscow.ru/rnews.html?id=65592).
