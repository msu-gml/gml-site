Active: yes
Title: Test page
Category: test_page
Id: test_page

## Admission

## Заголовки:
# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

# Вот теперь длинные заголовки выглядят хорошо, отступы между строк не представляются слишком большими. Это можно проверить, если зайти на страничку какой-нибудь новости с длинным названием.
## Вот теперь длинные заголовки выглядят хорошо, отступы между строк не представляются слишком большими. Это можно проверить, если зайти на страничку какой-нибудь новости с длинным названием.
### Вот теперь длинные заголовки выглядят хорошо, отступы между строк не представляются слишком большими. Это можно проверить, если зайти на страничку какой-нибудь новости с длинным названием.
#### Вот теперь длинные заголовки выглядят хорошо, отступы между строк не представляются слишком большими. Это можно проверить, если зайти на страничку какой-нибудь новости с длинным названием.
##### Вот теперь длинные заголовки выглядят хорошо, отступы между строк не представляются слишком большими. Это можно проверить, если зайти на страничку какой-нибудь новости с длинным названием.
###### Вот теперь длинные заголовки выглядят хорошо, отступы между строк не представляются слишком большими. Это можно проверить, если зайти на страничку какой-нибудь новости с длинным названием. 

## Начертание:

*test text*

**test text**

***test text***

~~test text~~
~~перечеркнутый текст~~

`test text` - обрамление текста 


## Списки:

>!!!!
>Может, стоит все-таки без красной строки, а с отступом между абзацами? Иначе эта красная строка встает там где надо и где не надо. Можно ее, конечно, попробовать убрать там, где не надо, но технически это будет сложнее. И визуально, возможно, не очень. 

List:  

* first

* second




List:

1. first

2. second

3. second

4. second

5. second

6. second

7. second

8. second

9. second

10. second

11. second

12. second


## Текст:

Graphics & Media Lab was established as part of department of computational mathematics and cybernetics of M.V. Lomonosov Moscow State University in 1998 and officially confirmed by dean order in 2002. The founder of laboratory was Prof. Yuri. M. Bayakovski, one of the pioneers of computer graphics in Russia and member of ACM Siggraph Computer Graphics Pioneers Club.


The main goals of laboratory are education and research in computer graphics, computer vision, image and video processing. Graphics & Media Lab is an active participant and one of the main organizers of annual interational conference GraphiCon (www.graphicon.ru), which is a leading conference on computer graphics and vision in Russia.

The head of laboratory is Dr. Anton S. Konushin.

Education in computer graphics in Lomonosov Moscow State University started in 1983. Since 1994 the course "Computer graphics" is mandatory for all students of department of computational mathematics and cybernetics. Currently members of our lab also teach several courses: "Introduction in computer vision", "Topics in computer vision", "Photorealistic image synthesis", "Video processing and compression", "Introduction to medical image analysis".

Research is currently supported by Intel, Microsoft Research, grants by Russian fund for basic research (RFBR), grants of the President of Russian Federation, state contracts, etc. 

## Цитаты:

>Blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote 
>>Blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote 
>>>Blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote blockquote 


## Ссылки:

[Old lab site](https://graphics.cs.msu.ru/)

[MSU site](https://www.msu.ru/)

## Изображения:

> И здесь ненужный отступ перед изображением. Пока не нашла, как убрать (Катя)
Изображения отцентрировала. При центровке отступ почти не заметен.

![Alt-текст](https://faizov_boris.gitlab.io/new-gml-site-arcana/images/projects/traffic_sign_recognition_small.jpg "Example image")

![Alt-текст](https://graphics.cs.msu.ru/sites/default/files/research/trafficsigns/trafficsign_example.jpg "Middle image")

> Теперь большие изображения ресайзятся под размер экрана.

![Big image](https://i1.wallbox.ru/wallpapers/main/201311/e506dfe667b1148.jpg "Big image")

## Код и формулы:



$$e=mc^2$$

Equation example: $e=mc^2$

```python
# Leverage the OS package (possible race condition)
import os 
my_dict = {"color": "red", "width": 17, "height": 19}
value_to_find = "red"
# Statically defined list
my_list = [2, 5, 6]
# Brute force solution (fastest) -- single key
for key, value in my_dict.items():
    if value == value_to_find:
        print(f'{key}: {value}')
        break
# Brute force with a try-except block (Python 3+)
try: 
    with open('/path/to/file', 'r') as fh:
        pass
except FileNotFoundError: 
    pass
```

```cpp
// Comment
#include <iostream>
class MyClass {
public:
    MyClass() {
        Foo();
    }
};
int main() {
  int y = SOME_MACRO_REFERENCE;
  int x = 5 + 6;
  cout << "Hello World! " << x << std::endl();
}
```

$$a=b$$ 

 \begin{equation} \label{eq} X^2 \end{equation}


$$α_t(i) = P(O_1, O_2, … O_t, q_t = S_i λ)$$

## Таблицы:

>Таблицы тоже центрирую

Human identification in video surveillance data is an important problem for security and counteraction to terrorism. However, the most common and widespread methods of biometrical identification, such as face recognition have limited application and are not usable in many real situations. For example, if several people wearing the same clothes (uniform) and having their faces hidden are observed, the only way to distinguish them from each other is by their gait, gestures, or build.

The gait recognition methods have been investigated since mid-nineties. The most promising approach achieving the best recognition quality is the 3D model approach, estimating static and dynamic features of human limbs motion in three-dimensional coordinates. The evaluation of these features requires human pose estimation, i.e. the position of all the main joints at each moment of time. Currently, it can be measured only with 3D sensors or multi-view systems working in controlled conditions. The gait recognition within the framework of video surveillance is impossible because of the low accuracy of human pose estimation in video. 

| Заголовок таблицы 1 | Заголовок 2 |
| ------------------- |:-----------:| 
|![Alt-текст](https://faizov_boris.gitlab.io/new-gml-site-arcana/images/projects/traffic_sign_recognition_small.jpg "Image 1") | ![Alt-текст](https://faizov_boris.gitlab.io/new-gml-site-arcana/images/projects/traffic_sign_recognition_small.jpg "Image 2") |

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |


####Таблица размеров обуви

The main goal of the project is the development of pose evaluation methods based on generative approach to improve the quality of pose estimation and the accuracy of gait and gesture recognition. The generative approach also allows to estimate the parameters of humans figure that can be used for build recognition. The second goal of the project is the investigation of neural network methods of gait recognition, that have already been successful in face recognition problem, but have not been applied to gait yet.

|Россия         |Великобритания |Европа |Длина ступни, см|
| ------------- |:-------------:| -----:| --------------:|
|34,5|3,5|36|23|
|35,5|4|36⅔|23–23,5|
|36|4,5|37⅓|23,5|
|36,5|5|38|24|
|37|5,5|38⅔|24,5|
|38|6|39⅓|25|
|38,5|6,5|40|25,5|
|39|7|40⅔|25,5–26|
|40|7,5|41⅓|26|
|40,5|8|42|26,5|
|41|8,5|42⅔|27|
|42|9|43⅓|27,5|
|43|9,5|44|28|
|43,5|10|44⅔|28–28,5|
|44|10,5|45⅓|28,5–29|
|44,5|11|46|29|
|45|11,5|46⅔|29,5|
   
Очень широкая таблица:

|Россия         |Украина |Белоруссия |Великобритания |Германия |Франция |Италия |Великобритания |Европа |Длина ступни, см|Великобритания |Германия |Франция |Италия |Великобритания |Европа |Длина ступни, см|
| ------------- |:------:| ---------:| -------------:| -------:| ------:| -----:|  ------------:| -----:|---------------:| -------------:| -------:| ------:| -----:|  ------------:| -----:|---------------:|
|34,5|3,5|36|23|3,5|36|23|3,5|36|23|23|3,5|36|23|3,5|36|23|
|35,5|4|36⅔|23–23,5|4|36⅔|23–23,5|4|36⅔|23–23,5|23–23,5|4|36⅔|23–23,5|4|36⅔|23–23,5|
|36|4,5|37⅓|23,5|4,5|37⅓|23,5|4,5|37⅓|23,5|23,5|4,5|37⅓|23,5|4,5|37⅓|23,5|

## Содержание страницы:

[TOC]
А вот \\[TOC\\] - и не должно сработать

## Пример видео:
{% youtube UVe9DVuxqGg [315] [560] %}

{% youtube UVe9DVuxqGg %}

Info about admission in Lab.
