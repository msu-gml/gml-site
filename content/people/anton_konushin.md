Active: yes
Category: people
PersonType: staff
Id: anton_konushin
Name: Anton
Surname: Konushin
PublicationsPossibleNames: Антон
PublicationsPossibleSurnames: Konouchine, Конушин
ParseNewPublications: yes
LabGroups: cv_group
PersonOrder: 5
Position: Associate professor
Email: anton.konushin@graphics.cs.msu.ru
IstinaPage: https://istina.msu.ru/profile/KonushinAS/
GoogleScholarPage: https://scholar.google.com/citations?user=ZT_k-wMAAAAJ
Photo: images/people/anton_konushin.jpg
ResearchInterests: computer vision, video surveillance, image-based 3D modeling, semantic analysis of images and 3D point clouds
Projects: traffic_sign_recognition, human_gait_recognition_in_video, traffic_sign_recognition_with_synthetic_datasets

I am an associate professor of Graphics & Media Lab.

I am also an [associate professor](https://www.hse.ru/en/staff/akonushin) in
National Research University Higher School of Economics.

I recieved my Ph.D. from Keldysh Institute for Applied Mathematics Russian
Academy of Science in 2005. I joined Lomonosov Moscow State University in 2005.
Since 2010 I’m also a lecturer in Yandex school for data analysis. In 2014 I
joined National Research University Higher School of Economics.

I’m also a scientific consultant in [“Video Analysis Technologies” LLC](http://www.tevian.ru/en), which
is a resident of Skolkovo IT cluster.
