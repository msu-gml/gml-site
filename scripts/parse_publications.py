# -*- coding: utf-8 -*-
import os
import json
import bibtexparser
import functools
import copy
import transliterate
import string
import argparse
from pelican.readers import MarkdownReader
from pelican.settings import read_settings


def SplitMdList(list_str):
    if list_str.strip() == '':
        return []
    else:
        return list(list_str.strip().split(', '))


def bib_papers_comparator(x, y):
    xy = str(x['year']).strip()
    yy = str(y['year']).strip()
    if xy < yy:
        return 1
    elif xy > yy:
        return -1
    else:
        return 0


def get_person_id(publ_name, info_for_people_parsing):
    for person_info in info_for_people_parsing:
        for surname in person_info['person_surnames']:
            pos = publ_name.find(surname)
            if pos == -1:
                continue
            find_name_str = publ_name[:pos] + publ_name[pos + len(surname):]
            for name in person_info["possible_names"]:
                if name[0] in find_name_str:
                    return person_info["person_id"]
    return None


def process_name(publ_name):
    original_name = list(publ_name.split(','))[:2]
    for i in range(len(original_name)):
        original_name[i] = string.capwords(original_name[i].strip().lower())
    if len(original_name) > 1:
        original_name[1] = original_name[1][:1] + '.'
        original_name[0], original_name[1] = original_name[1], original_name[0]
    original_name = ' '.join(original_name)
    return original_name


def gen_short_id(publication_entry, existing_ids):
    name_part = publication_entry['paperauthors'].split()[0].lower()
    year_part = str(publication_entry['year'])[-2:]
    title_parts = []
    for word in list(publication_entry['title'].lower().split()):
        word = transliterate.translit(word, 'ru', reversed=True)
        word = word.translate(str.maketrans('', '', string.punctuation))
        if len(word) > 4:
            title_parts.append(word)
    name_part = transliterate.translit(name_part, 'ru', reversed=True)
    name_part = name_part.translate(str.maketrans('', '', string.punctuation))
    cur_new_id = name_part + year_part + title_parts[0]
    i = 0
    while cur_new_id in existing_ids:
        i += 1
        cur_new_id += title_parts[i]
    return cur_new_id


def process_bad_sequences(bib_entry):
    for field in bib_entry.keys():
        for bad_seq in ['{\\\\flqq}', '{\\flqq}', '{\\\\frqq}', '{\\frqq}']:
            bib_entry[field] = bib_entry[field].replace(bad_seq, '"')
    return bib_entry


def update_publicaions(args, info_for_people_parsing):
    with open(args.old_json_path, "r") as fp:
        publications = json.load(fp)
    title_to_previous_publications = {
        publication['title'].strip(): i for i, publication in enumerate(publications)
    }
    existing_ids = [publication['id'] for publication in publications]
    with open(args.old_bib_path, 'r') as fp:
        bib_database = bibtexparser.bparser.BibTexParser().parse_file(fp)
    bibtex_id_to_previous_bibs = {
        bib["ID"]: i for i, bib in enumerate(bib_database.entries)
    }

    new_publications = []

    with open(args.update_bib_path, 'r') as fp:
        upd_bib_database = bibtexparser.bparser.BibTexParser().parse_file(fp)
        for bib_entry in upd_bib_database.entries:
            bib_entry = process_bad_sequences(bib_entry)
            journal = ""
            if 'booktitle' in bib_entry:
                journal = bib_entry['booktitle']
            elif 'journal' in bib_entry:
                journal = bib_entry['journal']

            journal_link = ""
            if 'doi' in bib_entry:
                journal_link = "http://dx.doi.org/" + bib_entry['doi']

            abstract = ""
            if 'annote' in bib_entry:
                abstract = bib_entry['annote']

            new_entry = {
                "id": bib_entry['ID'],
                "date": bib_entry['year'] + "-01-01",
                "year": bib_entry['year'],
                "title": bib_entry['title'].strip(),
                "lab_group_ids": [args.update_group_id],
                "paperauthors": bib_entry['author'],
                "journal": journal,
                "journal_link": journal_link,
                "bib_tex_id": bib_entry['ID'],
                "pdf_path": "",
                "abstract": abstract
            }
            if new_entry["title"] in title_to_previous_publications:
                publication_pos = title_to_previous_publications[new_entry["title"]]
                bib_entry['ID'] = publications[publication_pos]["id"]
                continue
            bib_entry['ID'] = new_entry["id"] = new_entry["bib_tex_id"] = gen_short_id(new_entry, existing_ids)
            existing_ids.append(new_entry["id"])
            new_publications.append(new_entry)

    for i in range(len(new_publications)):
        publ_people = [elem.strip() for elem in new_publications[i]['paperauthors'].split(' and ')]
        new_publ_people = []
        for j, publ_name in enumerate(publ_people):
            original_name = process_name(publ_name)
            found_id = get_person_id(publ_name, info_for_people_parsing)
            new_publ_people.append({
                "original_name": original_name,
                "person_id": found_id  # None in case if not found person
            })
        new_publications[i]['paperauthors'] = new_publ_people

    if args.update_old_publications:
        for i in range(len(publications)):
            publ_people = publications[i]['paperauthors']
            new_publ_people = []
            for j, person_info in enumerate(publ_people):
                if person_info['person_id'] is None:
                    person_info['person_id'] = get_person_id(person_info['original_name'], info_for_people_parsing)
                new_publ_people.append(person_info)
            publications[i]['paperauthors'] = new_publ_people

    new_publications += publications
    new_publications.sort(key=functools.cmp_to_key(bib_papers_comparator))
    with open(args.new_json_path, "w") as fp:
        json.dump(new_publications, fp, indent=4, ensure_ascii=False)

    db = bibtexparser.bibdatabase.BibDatabase()
    db.entries = copy.copy(bib_database.entries)
    for elem in upd_bib_database.entries:
        if elem['ID'] not in bibtex_id_to_previous_bibs:
            db.entries.append(elem)
    with open(args.new_bib_path, "w") as fp:
        bibtexparser.dump(db, fp)


def parse_people_info():
    pelican_md_reader = MarkdownReader(settings=read_settings('../pelicanconf.py'))
    info_for_people_parsing = []
    for person_md_file in os.listdir('../content/people/'):
        _, person_md_info = pelican_md_reader.read(os.path.join('../content/people/', person_md_file))
        if person_md_info['parsenewpublications'].lower() != 'yes' or \
                person_md_info['active'].lower() != 'yes':
            continue
        info_for_people_parsing.append({
            'person_id': person_md_info['id'],
            'person_surnames': [person_md_info['surname'].strip()] + SplitMdList(person_md_info['publicationspossiblesurnames']),
            'possible_names': [person_md_info['name'].strip()] + SplitMdList(person_md_info['publicationspossiblenames']),
            'istina_path': person_md_info.get('istinapage', '').strip(),
            'group_ids': SplitMdList(person_md_info['labgroups'])
        })
    return info_for_people_parsing


def parse_args():
    parser = argparse.ArgumentParser(usage='python3 parse_publications.py <args>')
    parser.add_argument('--update_bib_path', help='Путь до .bib - файла, содержащего новые публикации',
                        type=str, required=True, default=None)
    parser.add_argument('--update_group_id', help='Id группы, к которой относятся новые публикации в файле UPDATE_BIB_PATH',
                        choices=["cv_group", "video_group", "computer_graphics_group", "biomedical_group"],
                        type=str, required=True, default=None)
    parser.add_argument('--update_old_publications', help='Флаг с тем, нужно ли обновлять старые публикации (вдруг дозаполнили часть авторов, например)',
                        type=bool, required=False, default=True)
    parser.add_argument('--old_json_path', help='Путь до .json - файла с прошлыми публикациями',
                        type=str, required=False, default="../content/publications/publications_info.json")
    parser.add_argument('--new_json_path', help='Путь, куда надо записать обновленный .json - файл',
                        type=str, required=False, default="../content/publications/publications_info.json")
    parser.add_argument('--old_bib_path', help='Путь до .bib - файла с прошлыми публикациями',
                        type=str, required=False, default="../content/publications/publications_bibtexs.bib")
    parser.add_argument('--new_bib_path', help='Путь, куда надо записать обновленный .bib - файл',
                        type=str, required=False, default="../content/publications/publications_bibtexs.bib")
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    info_for_people_parsing = parse_people_info()
    update_publicaions(args, info_for_people_parsing)
